## Preparation

### Node version

Tested on

```
node --version                                                                                                                  ~/code/nodejs/slingfile_p
v11.10.1
```

### Create configuration file

Create file ```config.json``` with

```json
{
  "chrome": {
    "args": [
      "--no-sandbox",
      ",--disable-setuid-sandbox"
    ],
    "headless": false
  },
  "app": {
    "url": "http://yogahealthsecrets.com/",
    "save_dir": "save",
    "css_dir": "css_directory",
    "img_dir": "img_directory"
  }
}
```


### Install dependencies

Run command ```npm install```

## Running app

Run command ```npm start```