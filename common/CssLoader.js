'use strict';

const path = require('path');

const ResourceLoader = require('./ResourceLoader');
const ImageLoader = require('./ImageLoader');

class CssLoader extends ResourceLoader {
    constructor(options){
        super(options);
        this.save_directory = path.join('.', this.options.app.save_dir);
        this.root_directory = path.join(this.save_directory, this.options.app.css_dir);
    }

    async loadCss(url, root = true) {
        await this.init();
        let content = await this.loadResouce(url, 'text');
        let parser = new CssParser(this.options);
        content = await parser.parse(content, url);
        const full_path = await this.save(content, url);
        await this.exit();
        if (root) {
            return path.relative(this.root_directory, full_path);
        } else {
            return path.relative(this.save_directory, full_path);
        }
    }
}

class CssParser {
    constructor(options) {
        this.options = options;
    }

    static isCss(str) {
        return /\.css$/i.test(str);
    }

    static isImage(str) {
        return /\.(jpg|jpeg|png|gif)$/i.test(str);
    }

    async parse(content, base_url) {
        const urlRe = /url\('?([\w\/.-]+)'?\)/gi;
        const root_directory = path.join('.', this.options.app.save_dir);
        while (true) {
            const matches = urlRe.exec(content);
            if (!matches) break;
            const group = matches[1];
            const url = new URL(group, base_url);
            let local_path;
            if (CssParser.isCss(group)) {
                const loader = new CssLoader(this.options);
                local_path = await loader.loadCss(url);
            } else if (CssParser.isImage(group)) {
                const loader = new ImageLoader(this.options);
                local_path = await loader.loadImage(url, false);
                local_path = path.relative(root_directory, local_path);
            } else {
                // Unknown type.
                local_path = url;
            }
            content = content.replace(group, local_path);
        }
        return content;
    }
}

module.exports = CssLoader;