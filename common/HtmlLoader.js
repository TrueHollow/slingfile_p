'use strict';

const path = require('path');

const cheerio = require('cheerio');

const ResourceLoader = require('./ResourceLoader');
const CssLoader = require('./CssLoader');

class HtmlLoader extends ResourceLoader {
    constructor(options){
        super(options);
        this.root_directory = path.join('.', this.options.app.save_dir);
    }

    async run() {
        await this.init();
        const html = await this.parseContent();
        const link = this.options.app.url;
        await this.save(html, link);
    }

    async parseContent() {
        const original_url = new URL(this.options.app.url);
        const html = await this.loadResouce(original_url);
        const $ = cheerio.load(html);
        const origin = original_url.origin;
        await this.parseAll_tagA($, origin);
        await this.parseAll_tagLink($, origin);

        return $.html();
    }

    async parseAll_tagA($, base_url) {
        $('a').each((i, elem) => {
            const el = $(elem);
            const url = new URL(el.attr('href'), base_url);
            el.attr('href', url.href);
        });
    }

    async parseAll_tagLink($, base_url) {
        const css = [];
        $('link').each((i, elem) => {
            const el = $(elem);
            const url = new URL(el.attr('href'), base_url);
            css.push(url);
        });

        let promises = css.map((url) => {
            let cssLoader = new CssLoader(this.options);
            return cssLoader.loadCss(url, false);
        });

        let local_urls = await Promise.all(promises);
        $('link').each((i, elem) => {
            const el = $(elem);
            el.attr('href', local_urls[i]);
        });
    }
}

module.exports = HtmlLoader;