'use strict';

const path = require('path');

const ResourceLoader = require('./ResourceLoader');

class ImageLoader extends ResourceLoader {
    constructor(options) {
        super(options);
        this.save_directory = path.join('.', this.options.app.save_dir);
        this.root_directory = path.join(this.save_directory, this.options.app.img_dir);
    }

    async loadImage(url, root = true) {
        await this.init();
        let content = await this.loadResouce(url, 'binary');
        const full_path = await this.save(content, url);
        await this.exit();
        if (root) {
            return path.relative(this.root_directory, full_path);
        } else {
            return path.relative(this.save_directory, full_path);
        }
    }
}

module.exports = ImageLoader;