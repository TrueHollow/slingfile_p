'use strict';

const fs = require('fs');
const path = require('path');

const puppeteer = require('puppeteer');

const fileRegex = /[\w-]+?\.\w+?$/;

class ResourceLoader {
    constructor(options) {
        this.options = options;
    }

    static get fileRegex() {
        return fileRegex
    }

    async init() {
        this.browser = await puppeteer.launch(this.options.chrome);
    }

    async exit() {
        if (this.browser) {
            return this.browser.close();
        }
    }

    async save(content, url_content, file_name = 'index.html') {
        const url = new URL(url_content);
        let pathname = url.pathname;
        if (!ResourceLoader.fileRegex.test(pathname)) {
            pathname += file_name;
        }
        const full_path_dir = path.join(this.root_directory, pathname.replace(ResourceLoader.fileRegex, ''));
        const full_path_file = path.join(this.root_directory, pathname);

        await this.createDirectory(full_path_dir);
        await this.saveFile(full_path_file, content);
        return full_path_file;
    }

    async createDirectory(full_path) {
        return new Promise(((resolve, reject) => {
            fs.mkdir(full_path, {recursive: true}, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        }));
    }

    async saveFile(full_path, content) {
        return new Promise(((resolve, reject) => {
            fs.writeFile(full_path, content, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        }));
    }

    async loadResouce(url, type = 'html') {
        const page = await this.browser.newPage();
        let result;
        if (type !== 'html') {
            page.once('response', async (response) => {
                switch (type) {
                    case 'json':
                        result = await response.json();
                        break;
                    case 'text':
                        result = await response.text();
                        break;
                    case 'binary':
                        result = await response.buffer();
                        break;
                }
            });
        }
        await page.goto(url, {waitUntil: 'networkidle2'});
        switch (type) {
            case 'html':
                result = await page.content();
                break;
        }
        return result;
    }
}

module.exports = ResourceLoader;