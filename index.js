'use strict';

const config = require('./config');
const HtmlLoader = require('./common/HtmlLoader');

const options = config;
let app;

const run = () => {
    app = new HtmlLoader(options);
    return app.run();
};

run().then(async () => {
    await app.exit();
    console.log('Finished.')
});



